install base packages:
  pkg.latest:
    - pkgs:
      {% if grains['os'] in [ 'RedHat', 'CentOS', 'Fedora'] %}
      - clamav
      - clamav-data
      - clamav-update
      - slirp4netns
      - kubectl
      - sysdig
      - tmux
      - parted
      {% endif %}

Install packages from Web:
  pkg.installed:
    - sources:
      - dive: https://github.com/wagoodman/dive/releases/download/v0.9.2/dive_0.9.2_linux_amd64.rpm

Add LVM Volume:
  lvm.lv_present:
    - name: testbed
    - vgname: fedora_fedora
    - size: 250G

Make filesystem for volume:
  module.run:
    - name: extfs.mkfs
    - device: /dev/fedora_fedora/testbed
    - fs_type: ext4

Make mountpoint:
  file.directory:
    - name: /opt/testbed
    - user: root
    - group: root
    - dir_mode: 777
    - file_mode: 777
    - recurse:
      - user
      - group
      - mode

Mount new volume:
  mount.fstab_present:
    - name: /dev/fedora_fedora/testbed
    - fs_file: /opt/testbed
    - fs_vfstype: ext4

Add Kubectl Bash Completion:
  file.append:
    - name: /etc/bashrc
    - text:
      - source <(kubectl completion bash)

Add Umoci:
  file.managed:
    - name: /usr/bin/umoci
    - source: salt://files/umoci.amd64
    - mode: 755
    - skip_verify: True

Add 7zip:
  file.managed:
    - name: /usr/bin/7zz
    - source: salt://files/7zz
    - mode: 755
    - skip_verify: True

Add 7zip Aliases:
  file.managed:
    - name: /etc/profile.d/7zip.sh
    - mode: 755
    - replace: True
    - skip_verify: True
    - contents:
      - alias 7z="/usr/bin/7zz"
      - alias 7za="/usr/bin/7zz"

Make docker tmp directory:
  file.directory:
    - name: /opt/testbed/tmp
    - user: root
    - group: root
    - dir_mode: 777
    - file_mode: 777
    - recurse:
      - user
      - group
      - mode

Make docker tmp symlink:
  file.symlink:
    - name: /home/vagrant/.local/share/docker/tmp
    - target: /opt/testbed/tmp
    - user: vagrant
    - group: vagrant
    - force: True

Make Images directory:
  file.directory:
    - name: /opt/testbed/images
    - user: root
    - group: root
    - dir_mode: 777
    - file_mode: 777
    - recurse:
      - user
      - group
      - mode

Make Dynamic Archive directory:
  file.directory:
    - name: /opt/testbed/dynamic
    - user: root
    - group: root
    - dir_mode: 777
    - file_mode: 777
    - recurse:
      - user
      - group
      - mode

Make Logs directory:
  file.directory:
    - name: /opt/testbed/logs
    - user: root
    - group: root
    - dir_mode: 777
    - file_mode: 777
    - recurse:
      - user
      - group
      - mode

Make tools directory:
  file.directory:
    - name: /home/vagrant/analysis
    - user: vagrant
    - group: vagrant
    - dir_mode: 777
    - file_mode: 777
    - recurse:
      - user
      - group
      - mode

Make Dsvw directory:
  file.directory:
    - name: /home/vagrant/dsvw
    - user: vagrant
    - group: vagrant
    - dir_mode: 777
    - file_mode: 777
    - recurse:
      - user
      - group
      - mode

Add docker_export.sh:
  file.managed:
    - name: /home/vagrant/analysis/docker_export.sh
    - source: salt://files/docker_export.sh
    - mode: 755
    - skip_verify: True

Add docker_extract.sh:
  file.managed:
    - name: /home/vagrant/analysis/docker_extract.sh
    - source: salt://files/docker_extract.sh
    - mode: 755
    - skip_verify: True

Add Static Analysis Shell:
  file.managed:
    - name: /home/vagrant/analysis/static_analysis.sh
    - source: salt://files/static_analysis.sh
    - mode: 755
    - skip_verify: True

Add Static Analysis Python:
  file.managed:
    - name: /home/vagrant/analysis/static_analysis_report.py
    - source: salt://files/static_analysis_report.py
    - mode: 755
    - skip_verify: True

Add Dynamic Analysis Shell:
  file.managed:
    - name: /home/vagrant/analysis/dynamic_analysis.sh
    - source: salt://files/dynamic_analysis.sh
    - mode: 755
    - skip_verify: True

Add Dynamic Analysis Python:
  file.managed:
    - name: /home/vagrant/analysis/dynamic_analysis_report.py
    - source: salt://files/dynamic_analysis_report.py
    - mode: 755
    - skip_verify: True

Make Dsvw directory:
  file.recurse:
    - name: /home/vagrant/dsvw
    - source: salt://files/dsvw
    - user: vagrant
    - group: vagrant
    - dir_mode: 777
    - file_mode: 777

Update Clamav Db:
  cmd.run:
    - name: 'freshclam'

Run Clamav:
  cmd.run:
    - name: 'clamscan -irv --log=/root/scan_vmbuild.log --exclude=\"^/sys\" / || true'
