#!/usr/bin/env bash

logDir="/opt/testbed/logs"
rm -rf ${logDir}/static/*.log 2>&1 > /dev/null;

mkdir -p ${logDir}/static 2>&1 > /dev/null;

for extractedImage in $(ls -d /opt/testbed/extracted/*); do
  imgName=$(basename ${extractedImage});
  clamscan -irv --log=${logDir}/static/${imgName}_av.log ${extractedImage} || true
done;

for img in $(find /opt/testbed/images/ -type f -name *.tar); do
  export img_name=$(basename $img | sed 's/.tar//g')
  trivy image -f table --input "${img}" 2>&1 | tee  ${logDir}/static/${img_name}_trivy_table_full.log;
  trivy image -f json --input "${img}" 2>/dev/null | egrep -v "INFO|WARN" | tee  ${logDir}/static/${img_name}_trivy_json_full.log
  unset img_name
done;

for img in $(find /opt/testbed/images/ -type f -name *.tar); do
  export img_name=$(basename $img | sed 's/.tar//g')
  grype "${img}" -o table 2>&1 | tee  ${logDir}/static/${img_name}_grype_table_full.log;
  grype "${img}" -o json 2>/dev/null | tee  ${logDir}/static/${img_name}_grype_json_full.log
  unset img_name
done;
