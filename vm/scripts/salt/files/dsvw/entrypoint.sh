#!/usr/bin/env sh

## This application is written in Pyton 2
## and would require some updates to function 
## in Python 3
#python3 /dsvw.py &

# This should trigger an issue wioth teh anti-virus
wget https://secure.eicar.org/eicar.com.txt

# generate some packet data as if a cryptominer was added without detection
trap 'exit 0' SIGTERMi

while true; do
  ping -c 6 8.8.8.8 > /var/log/miner_data;
  sleep 15;
done
