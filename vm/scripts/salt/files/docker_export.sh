#!/usr/bin/env bash
logDir="/opt/testbed/logs"
rm -rf /opt/testbed/images/*;

for img in $(docker image list | awk '{print $1}' | grep -iv repository); do
  mkdir -p "/opt/testbed/images/$(basename $img)" && docker image save -o "/opt/testbed/images/$(basename $img)/$(basename $img).tar" "${img}" && rm -rf /home/vagrant/.local/share/docker/tmp/* > /dev/null && printf "/opt/testbed/images/$(basename $img)/$(basename $img).tar created...\n" | tee -a ${logDir}/export.log;
done
