printf "\n\n# Per https://www.ssi.gouv.fr/uploads/2019/03/linux_configuration-en-v1.2.pdf\n" >> /etc/sysctl.conf
printf "## Disable IPv6\n" >> /etc/sysctl.conf
printf "net.ipv6.conf.all.disable_ipv6 = 1\n" >> /etc/sysctl.conf
printf "## Disable Packet redirects\n" >> /etc/sysctl.conf
printf "net.ipv4.conf.send_redirects = 0\n" >> /etc/sysctl.conf
printf "net.ipv4.conf.default.send_redirects = 0\n" >> /etc/sysctl.conf
printf "net.ipv4.conf.all.accept_redirects = 0\n" >> /etc/sysctl.conf
printf "net.ipv4.conf.all.secure_redirects = 0\n" >> /etc/sysctl.conf
printf "net.ipv4.conf.default.accept_redirects = 0\n" >> /etc/sysctl.conf
printf "net.ipv4.conf.default.secure_redirects = 0\n" >> /etc/sysctl.conf
printf "## Log Packets with abnormal IP's\n" >> /etc/sysctl.conf
printf "net.ipv4.conf.all.log_martians = 1\n" >> /etc/sysctl.conf
printf "## RFC 1337\n" >> /etc/sysctl.conf
printf "net.ipv4.tcp_rfc1337 = 1\n" >> /etc/sysctl.conf
printf "## Ignor bogus ICMP responses\n" >> /etc/sysctl.conf
printf "net.ipv4.icmp_ignore_bogus_error_responses = 1\n\n" >> /etc/sysctl.conf
#####
#Preconfig - Rootless Docker
printf "user.max_user_namespaces=28633\n" >> /etc/sysctl.d/99-rootless.conf
#add ping for users
printf "net.ipv4.ping_group_range = 0 2147483647\n" >> /etc/sysctl.d/99-rootless.conf
# Allow users to view all ports
printf "net.ipv4.ip_unprivileged_port_start=0\n" >> /etc/sysctl.d/99-rootless.conf
# Add vagrant user access to subuid and subgid
#printf "vagrant:100000:65536\n" >> /etc/subuid
echo "vagrant:100000:65536" >> /etc/subuid
echo "vagrant:100000:65536" >> /etc/subgid
sysctl --system

printf "if [[ \$XDG_RUNTIME_DIR ]]; then\n\texport DOCKER_HOST=unix://\$XDG_RUNTIME_DIR/docker.sock\nfi;" >> /etc/profile.d/docker_sock.sh

sed -i "s/^.*GRUB_CMDLINE_LINUX=\"/GRUB_CMDLINE_LINUX=\"systemd.unified_cgroup_hierarchy=1 /" /etc/default/grub;
grub2-mkconfig -o /boot/grub2/grub.cfg

mkdir -p /etc/systemd/system/user@.service.d
printf "[Service]\nDelegate=cpu cpuset io memory pids" >> /etc/systemd/system/user@.service.d/delegate.conf
