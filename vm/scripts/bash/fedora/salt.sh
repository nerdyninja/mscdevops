major_version="`sed 's/^.\+ release \([.0-9]\+\).*/\1/' /etc/redhat-release | awk -F. '{print $1}'`";

# make sure we use dnf on EL 8+
if [ "$major_version" -ge 8 ]; then
  YUMUP=dnf
else
  YUMUP=yum
fi
# required for VirtualBox 4.3.26
$YUMUP install -y bzip2

# rpm -ivh http://mirror.pnl.gov/epel/8/Everything/x86_64/Packages/e/epel-release-8-7.el8.noarch.rpm;
$YUMUP install -y python3
$YUMUP install -y salt-master salt-minion salt-ssh salt-syndic salt-cloud salt-api

firewall-cmd --permanent --add-port={4505,4506}/tcp
firewall-cmd --reload
