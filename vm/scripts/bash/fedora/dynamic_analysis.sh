#!/usr/bin/env bash

archiveDir="/opt/testbed/dynamic";
logDir="/opt/testbed/logs"
mkdir -p ${logDir} > /dev/null;

#########
## DO NOT EDIT
########

trap "exit 0" SIGTERM

overlay="$HOME/.local/share/docker/fuse-overlayfs/"

scan() {
  name=$1
  img=$2

  printf "== Dynamic Analysis - ${img} as ${name} ==\n";
  ls -1 ${overlay} > overlay1.txt;

  sudo sysdig -w "${logDir}/dynamic/${name}.scap" &
  dig_proc=$!

  data=$(docker run -it -d --name ${name} ${img});
  printf "container: ${data}\n" | tee ${logDir}/dynamic/${name}_dynamic.log;

  ls -1 ${overlay} > overlay2.txt;
  running=$(diff overlay1.txt overlay2.txt | grep '>' | grep -iv "init" | awk -F' ' '{print $2}')
  tmpPath="${overlay}${running}/";

  printf "== Running Antivirus ==\n" | tee "${logDir}/dynamic/${name}_dynamic_av.log"; 
  clamscan -irv "${tmpPath}" | tee -a "${logDir}/dynamic/${name}_dynamic_av.log" || true
  av_proc=$!

  printf "\n== Pause for 90s ==\n" | tee -a "${logDir}/dynamic/${name}_dynamic_av.log"
  sleep 90;

  printf "== Running Antivirus again ==\n" | tee -a "${logDir}/dynamic/${name}_dynamic_av.log";
  clamscan -irv "${tmpPath}" | tee -a "${logDir}/dynamic/${name}_dynamic_av.log" || true

  printf "\n\n== Stopping ${name} ==\n" | tee -a ${logDir}/dynamic/${name}_dynamic.log; 
  docker stop ${name} > /dev/null;

  printf "== Archiving Container Runtime files ==\n" | tee -a ${logDir}/dynamic/${name}_dynamic.log;
  
  pushd "${tmpPath}" > /dev/null;
  tar -cJf "${archiveDir}/${name}.tar.xz" . | tee -a ${logDir}/dynamic/${name}_dynamic.log;
  zip -9 -y -r "${archiveDir}/${name}.zip" * | tee -a ${logDir}/dynamic/${name}_dynamic.log;
  popd > /dev/null

  rm -f overlay1.txt overlay2.txt;

  for prc in $(ps -ef | grep -i sysdig | grep -iv grep | awk -F' ' '{print $2}'); do sudo kill -15 $prc; done;
  wait;
  printf "\n\n============\nSysdig\n============\n" | tee -a ${logDir}/dynamic/${name}_dynamic.log;
  printf "Base IP Connectivity:\n" | tee -a ${logDir}/dynamic/${name}_dynamic.log;
  sysdig -r /opt/testbed/logs/dynamic/baseline.scap -pc -c topconns 2>&1 | tee -a ${logDir}/dynamic/${name}_dynamic.log;
  printf "\n\n${name} IP Connectivity:\n" | tee -a ${logDir}/dynamic/${name}_dynamic.log;
  sysdig -r "${logDir}/dynamic/${name}.scap" -pc -c topconns 2>&1 | tee -a ${logDir}/dynamic/${name}_dynamic.log;
  printf "\n\n" | tee -a ${logDir}/dynamic/${name}_dynamic.log;
  #sysdig -r exploit.scap -A fd.type="ipv4" and evt.type!=switch | tee -a ${logDir}/dynamic/${name}_dynamic.log;
}


create_baseline() {
  sudo sysdig -w "${logDir}/dynamic/baseline.scap" &
  dig_proc=$!

  netstat -tulnp | tee "${logDir}/dynamic/baseline.log" 

  sleep 300; 

  for prc in $(ps -ef | grep -i sysdig | grep -iv grep | awk -F' ' '{print $2}'); do sudo kill -15 $prc; done;
  wait;
  ps -ef | grep -i sysdig | tee -a "${logDir}/dynamic/baseline.log"
}

create_baseline;

for run_img in $(docker image list | awk '{print $1}' | egrep -iv "repository|alpine"); do
    run_name=$(basename $run_img);

    scan $run_name $run_img;
    
    docker container rm -f $run_name
    
done;
docker container prune -f > /dev/null;

## Cleanup any leftover processes
for prc in $(ps -ef | grep -i sysdig | grep -iv grep | awk -F' ' '{print $2}'); do sudo kill -1 $prc; done;
wait;
for prc in $(ps -ef | grep -i sysdig | grep -iv grep | awk -F' ' '{print $2}'); do sudo kill -9 $prc; done;
wait;

