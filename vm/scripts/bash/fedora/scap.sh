#!/usr/bin/env bash

if [[ "$(whoami)" != "root" ]]; then
  printf "Sorry must be run as root or with sudo\n";
else
  dnf install -y openscap openscap-containers scap-security-guide
  mkdir -p /home/vagrant/scap/remediated
  chown -R vagrant:vagrant /home/vagrant/scap
  chmod +x /home/vagrant/scap
  pushd /home/vagrant/scap > /dev/null

  oscap xccdf eval --fetch-remote-resources --profile xccdf_org.ssgproject.content_profile_ospp --results-arf /home/vagrant/scap/arf.xml --remediate --results /home/vagrant/scap/results.xml --report /home/vagrant/scap/report.html --oval-results /usr/share/xml/scap/ssg/content/ssg-fedora-ds-1.2.xml

  pushd /home/vagrant/scap/remediated
  oscap xccdf eval --fetch-remote-resources --profile xccdf_org.ssgproject.content_profile_ospp --results-arf /home/vagrant/scap/arf.xml --remediate --results /home/vagrant/scap/results.xml --report /home/vagrant/scap/report.html --oval-results /usr/share/xml/scap/ssg/content/ssg-fedora-ds-1.2.xml
fi;
