#!/bin/sh -eux
# determine the major EL version we're runninng
major_version="`sed 's/^.\+ release \([.0-9]\+\).*/\1/' /etc/redhat-release | awk -F. '{print $1}'`";

# make sure we use dnf on EL 8+
if [ "$major_version" -ge 8 ]; then
  YUMUP=dnf
else
  YUMUP=yum
fi

sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers;

$YUMUP clean all;
$YUMUP repolist;
$YUMUP update -y;
$YUMUP upgrade -y;
$YUMUP -y install wget bzip2 dkms dbus-daemon shadow-utils;#
# Recommended by Docker
$YUMUP -y install fuse-overlayfs iptables;

loginctl enable-linger vagrant;
