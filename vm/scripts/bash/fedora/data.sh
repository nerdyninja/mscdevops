#!/usr/bin/env bash

if [[ "$(whoami)" != "root" ]]; then
  printf "Sorry must be run as root or with sudo\n";
else
  mkdir -p /opt/testbed/tmp
  #mkdir -p /opt/testbed/images
  #mkdir -p /opt/testbed/dynamic
  #mkdir -p /opt/testbed/logs
  mkdir -p /home/vagrant/analysis

  ln -s /opt/testbed/tmp /home/vagrant/.local/share/docker/tmp
  chown -R vagrant:vagrant /opt/testbed/tmp
  chown -R vagrant:vagrant /home/vagrant
  chmod 777 -R  /opt/testbed

  dnf install -y clamav clamav-data clamav-update slirp4netns kubectl sysdig tmux parted
  cp -r /home/vagrant/p7zip/* /usr/bin/;
fi;
