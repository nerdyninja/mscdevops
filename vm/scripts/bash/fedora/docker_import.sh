#!/usr/bin/env bash

now=$(date +%d-%m-%Y);
for img in $(find /opt/testbed/images/ -type f -name *.tar); do
  imgName=$(basename $img);
  pathName=$(echo ${imgName} | sed 's,\.tar,,g');

  #printf "${img}\n${imgName}\n${pathName}\n\n";
  docker image import ${img} ${pathName}:tested;
done;
docker image ls;
