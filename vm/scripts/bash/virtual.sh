#!/bin/sh -eux

# set a default HOME_DIR environment variable if not set
HOME_DIR="${HOME_DIR:-/home/vagrant}";

case "$PACKER_BUILDER_TYPE" in
virtualbox-iso|virtualbox-ovf)
    VER="`cat $HOME_DIR/.vbox_version`";
    ISO="VBoxGuestAdditions_$VER.iso";

    # mount the ISO to /tmp/vbox
    mkdir -p /tmp/vbox;
    mount -o loop $HOME_DIR/$ISO /tmp/vbox;

    echo "installing deps necessary to compile kernel modules"
    # We install things like kernel-headers here vs. kickstart files so we make sure we install them for the updated kernel not the stock kernel
    if [ -f "/bin/dnf" ]; then
        dnf install -y --skip-broken perl cpp gcc make bzip2 tar kernel-headers kernel-devel kernel-uek-devel || true # not all these packages are on every system
    elif [ -f "/bin/yum" ] || [ -f "/usr/bin/yum" ]; then
        yum install -y --skip-broken perl cpp gcc make bzip2 tar kernel-headers kernel-devel kernel-uek-devel || true # not all these packages are on every system
    fi

    echo "installing the vbox additions"
    # this install script fails with non-zero exit codes for no apparent reason so we need better ways to know if it worked
    /tmp/vbox/VBoxLinuxAdditions.run --nox11 || true

    if ! modinfo vboxsf >/dev/null 2>&1; then
         echo "Cannot find vbox kernel module. Installation of guest additions unsuccessful!"
         exit 1
    fi

    echo "unmounting and removing the vbox ISO"
    umount /tmp/vbox;
    rm -rf /tmp/vbox;
    rm -f $HOME_DIR/*.iso;

    echo "removing kernel dev packages and compilers we no longer need"
    if [ -f "/bin/dnf" ]; then
        dnf remove -y gcc cpp kernel-headers kernel-devel kernel-uek-devel
    elif [ -f "/bin/yum" ] || [ -f "/usr/bin/yum" ]; then
        yum remove -y gcc cpp kernel-headers kernel-devel kernel-uek-devel
    fi

    echo "removing leftover logs"
    rm -rf /var/log/vboxadd*
    ;;

vmware-iso|vmware-vmx)
    # make sure we have /sbin in our path. RHEL systems lack this
    PATH=/sbin:$PATH
    export PATH

    mkdir -p /tmp/vmware;
    mkdir -p /tmp/vmware-archive;
    mount -o loop $HOME_DIR/linux.iso /tmp/vmware;

    TOOLS_PATH="`ls /tmp/vmware/VMwareTools-*.tar.gz`";
    VER="`echo "${TOOLS_PATH}" | cut -f2 -d'-'`";
    MAJ_VER="`echo ${VER} | cut -d '.' -f 1`";

    if [ -f "/bin/dnf" ]; then
        echo "Installing deps for the vmware tools"
        dnf install -y perl gcc make kernel-headers kernel-devel
    elif [ -f "/bin/yum" ] || [ -f "/usr/bin/yum" ]; then
        echo "Installing deps for the vmware tools"
        yum install -y perl gcc make kernel-headers kernel-devel
    fi

    echo "VMware Tools Version: $VER";
    echo "Expanding the tools archive"
    tar xzf ${TOOLS_PATH} -C /tmp/vmware-archive;
    echo "Installing tools"
    if [ "${MAJ_VER}" -lt "10" ]; then
        /tmp/vmware-archive/vmware-tools-distrib/vmware-install.pl --default;
    else
        /tmp/vmware-archive/vmware-tools-distrib/vmware-install.pl --force-install;
    fi
    umount /tmp/vmware;
    rm -rf  /tmp/vmware;
    rm -rf  /tmp/vmware-archive;
    rm -f $HOME_DIR/*.iso;

    # the proprietary vm tools don't work on Fedora 30 so we'll install the open-vm-tools
    dnf install -y open-vm-tools
    mkdir /mnt/hgfs;
    systemctl enable vmtoolsd
    systemctl start vmtoolsd
    ;;
esac
