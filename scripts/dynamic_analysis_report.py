#!/usr/bin/env python

import json, os

av = []
out_data = []

with os.scandir('/opt/testbed/logs/dynamic') as it:
    for entry in it:
        if '_av.log' in entry.name:
            av.append("/opt/testbed/logs/dynamic/{}".format(entry.name))

av.sort()

outfile = open('dynamic_table.tex', 'w')

outfile.write("\\begin{table}[ht!]\n\\centering\n\\begin{tabular}{ |c|c|c| } \\hline\n")
outfile.write("\\multicolumn{3}{|c|}{Antivirus} \\\\ \\hline\n")
outfile.write("Container & Scanned & Infected \\\\ \\hline\n")
found_text = "\\begin{table}[ht!]\n\\centering\n\\begin{tabular}{ |c|c| } \\hline\n"
found_text = found_text + "\\multicolumn{2}{|c|}{Antivirus - Issues Found} \\\\ \\hline\n"
found_text = found_text + "Container & Details \\\\ \\hline\n"

for myFile in av:
    container = myFile.split('_av.log')[0].split('dynamic/')[1].split('_')[0]
    scanned = ''
    infected = ''
    infected_list = []

    fileData = open(myFile,'r') 

    for line in fileData:
        if 'Scanned files' in line:
            scanned = line.split(':')[1].strip()
        elif 'Infected files' in line:
            infected = line.split(':')[1].strip()
        elif 'FOUND' in line:
            if line.strip() not in infected_list:
                infected_list.append(line.strip())
    
    data = ""
    for entry in infected_list:
        data = data + entry + "\n"
    
    data = data.replace("_","\_").replace("$","\$").replace("^","\string^").strip()

    if len(infected_list) > 1:
        found_text = found_text + "{} & \\multirow{{{}}}{} \\\\ \\hline\n".format(container,len(infected_list),data)
    elif len(infected_list) > 0:
        found_text = found_text + "{} & {} \\\\ \\hline\n".format(container,data)

    outfile.write("{} & {} & {} \\\\ \\hline\n".format(container,scanned,infected))  

outfile.write("\\end{tabular}\n\\caption{Antivirus - Dynamic Analysis}\\label{table:4}\\end{table}\n\n")
found_text = found_text + "\\end{tabular}\n\\caption{Antivirus - Dynamic Analysis Details}\\label{table:5}\\end{table}\n\n"
outfile.write("\n\n{}\n\n".format(found_text))
