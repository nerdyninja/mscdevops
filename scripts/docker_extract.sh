#!/usr/bin/env bash

logDir="/opt/testbed/logs"
rm -rf /opt/testbed/tmp/extract/* 2>&1 > /dev/null;

now=$(date +%d-%m-%Y);
printf "###########################\n" > ${logDir}/extract.log;
printf "###  Extracting ${date} \n" >> ${logDir}/extract.log;
printf "###########################\n" >> ${logDir}/extract.log;

for img in $(find /opt/testbed/images/ -type f -name *.tar); do
  mkdir -p "/opt/testbed/tmp/extract/$(basename $img)";
  7za x -snh -snl -o"/opt/testbed/tmp/extract/$(basename $img)" "${img}" 2>&1 | tee -a ${logDir}/extract.log;

  imgName=$(basename $img);
  pathName=$(echo ${imgName} | sed 's,\.tar,,g');
  mkdir -p "/opt/testbed/extracted/${pathName}";
  pushd "/opt/testbed/extracted/${pathName}" > /dev/null;

  for tarArch in $(find /opt/testbed/tmp/extract/$(basename $img) -type f -name *.tar); do
  #  printf "7za x -y -snh -snl -o/opt/testbed/extracted/${pathName} \"${tarArch}\" 2>&1 | tee -a ${logDir}/extract.log\n" | tee -a ${logDir}/extract.log;
  #  7za x -y -snh -snl -o/opt/testbed/extracted/${pathName} "${tarArch}" 2>&1 | tee -a ${logDir}/extract.log
    printf "tar xvf \"${tarArch}\" 2>&1 | tee -a ${logDir}/extract.log;\n" | tee -a ${logDir}/extract.log
    tar xhvf "${tarArch}" 2>&1 | tee -a ${logDir}/extract.log;
  done;
  popd > /dev/null;
done;
