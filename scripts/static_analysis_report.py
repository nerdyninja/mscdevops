#!/usr/bin/env python

import json, os

trivy = []
grype = []
av = []
out_data = []

with os.scandir('/opt/testbed/logs/static') as it:
    for entry in it:
        if 'trivy_json' in entry.name:
            trivy.append("/opt/testbed/logs/static/{}".format(entry.name))
        if 'grype_json' in entry.name:
            grype.append("/opt/testbed/logs/static/{}".format(entry.name))
        if '_av.log' in entry.name:
            av.append("/opt/testbed/logs/static/{}".format(entry.name))

trivy.sort()
grype.sort()
av.sort()

outfile = open('/opt/testbed/logs/static/static_analysis.csv', 'w')
outfile2 = open('static_analysis.csv', 'w')
outfile3 = open('static_table.tex', 'w')
outfile.write('container,tool,issue,severity,installedVersion,fixedVersion,name\n')
outfile2.write('container,tool,issue,severity,installedVersion,fixedVersion,name\n')

print("---------------------------------------------------------------------------------------")
print("{:50s} {:5s} {:50s}".format(" ","Trivy"," "))
print("---------------------------------------------------------------------------------------")
print("{:50s}|{:23s}|{:23s}|Unknown".format("Container","Fixed","Unfixed"))
print("{:50s}|{:5s}|{:5s}|{:5s}|{:5s}|{:5s}|{:5s}|{:5s}|{:5s}|{:7s}".format(" ","C","H","M","L","C","H","M","L"," "))


outfile3.write("\\begin{table}[ht!]\n\\centering\n\\begin{tabular}{ |c|c|c|c|c|c|c|c|c|c|} \\hline\n")
outfile3.write("\\multicolumn{10}{|c|}{Trivy} \\\\ \\hline\n")
outfile3.write("Container & \\multicolumn{4}{|c|}{Fixed} & \\multicolumn{4}{|c|}{Unfixed} & Unknown \\\\ \\hline\n")
outfile3.write(" & C & H & M & L & C & H & M & L & \\\\ \\hline\n")
for myFile in trivy:
    container = myFile.split('_trivy_')[0].split('static/')[1]
    f = open(myFile,'r')
    resData = json.load(f)

    critical = 0
    critical_fixed = 0
    critical_unfixed = 0
    high = 0
    high_fixed = 0
    high_unfixed = 0
    medium = 0
    medium_fixed = 0
    medium_unfixed = 0
    low = 0
    low_fixed = 0
    low_unfixed = 0
    unknown = 0

    fixed = False

    if 'Vulnerabilities' in resData['Results'][0]:
        vuln = resData['Results'][0]['Vulnerabilities']
    else:
        vuln = []

    for data in vuln:
        severity = data['Severity']
        id = data['VulnerabilityID']
        installedVersion = data['InstalledVersion']
        name = data['PkgName']

        if 'FixedVersion' in data:
            fixedVersion = data['FixedVersion'].split(',')[0].strip()
            fixed = True

        if 'CRITICAL' in severity:
            critical = critical + 1
            if fixed:
                critical_fixed = critical_fixed + 1
            else:
                critical_unfixed = critical_unfixed + 1
        elif 'HIGH' in severity:
            high = high + 1
            if fixed:
                high_fixed = high_fixed + 1
            else:
                high_unfixed = high_unfixed + 1
        elif 'MEDIUM' in severity:
            medium = medium + 1
            if fixed:
                medium_fixed = medium_fixed + 1
            else:
                medium_unfixed = medium_unfixed + 1
        elif 'LOW' in severity:
            low = low + 1
            if fixed:
                low_fixed = low_fixed + 1
            else:
                low_unfixed = low_unfixed + 1
        else:
            unknown = unknown + 1

        if 'FixedVersion' in data:
            info = "{},{},{},{},{},{},{}".format(container,'trivy',id, severity.upper(), installedVersion, fixedVersion, name)
        else:
            info = "{},{},{},{},{},,{}".format(container,'trivy',id, severity.upper(), installedVersion, name)

        out_data.append(info)
    print("{:50s}|{:5d}|{:5d}|{:5d}|{:5d}|{:5d}|{:5d}|{:5d}|{:5d}|{:7d}".format(container,critical_fixed,high_fixed,medium_fixed,low_fixed,critical_unfixed,high_unfixed,medium_unfixed,low_unfixed,unknown))
    outfile3.write("{} & {} & {} & {} & {} & {} & {} & {} & {} & {} \\\\ \\hline\n".format(container,critical_fixed,high_fixed,medium_fixed,low_fixed,critical_unfixed,high_unfixed,medium_unfixed,low_unfixed,unknown) )
outfile3.write("\\end{tabular}\n\\caption{Trivy - Static Analysis}\\label{table:1}\\end{table}\n\n")

outfile3.write("\\begin{table}[ht!]\n\\centering\n\\begin{tabular}{ |c|c|c|c|c|c|c|c|c|c|} \\hline\n")
outfile3.write("\\multicolumn{10}{|c|}{Grype} \\\\ \\hline\n")
outfile3.write("Container & \\multicolumn{4}{|c|}{Fixed} & \\multicolumn{4}{|c|}{UnFixed} & Unknown \\\\ \\hline\n")
outfile3.write(" & C & H & M & L & C & H & M & L & \\\\ \\hline\n")
print("\n\n---------------------------------------------------------------------------------------")
print("{:50s} {:5s} {:50s}".format(" ","Grype"," "))
print("---------------------------------------------------------------------------------------")
print("{:50s}|{:23s}|{:23s}|Unknown".format("Container","Fixed","UnFixed"))
print("{:50s}|{:5s}|{:5s}|{:5s}|{:5s}|{:5s}|{:5s}|{:5s}|{:5s}|{:7s}".format(" ","C","H","M","L","C","H","M","L"," "))
for myFile in grype:
    critical = 0
    critical_fixed = 0
    critical_unfixed = 0
    high = 0
    high_fixed = 0
    high_unfixed = 0
    medium = 0
    medium_fixed = 0
    medium_unfixed = 0
    low = 0
    low_fixed = 0
    low_unfixed = 0
    unknown = 0

    container = myFile.split('_grype_')[0].split('static/')[1]
    f = open(myFile,'r')
    resData = json.load(f)

    for match in resData['matches']:
        fixed = match['vulnerability']['fix']['state']
        if fixed == 'fixed':
            fixedVersion = match['vulnerability']['fix']['versions'][0]

        id = match['vulnerability']['id']

        if len(match['relatedVulnerabilities']) == 0:
            severity = match['vulnerability']['severity']
        else:
            severity = match['relatedVulnerabilities'][0]['severity']

        name = match['artifact']['name']
        installedVersion = match['artifact']['version']

        if 'Critical' in severity:
            critical = critical + 1
            if fixed == 'fixed':
                critical_fixed = critical_fixed + 1
            else:
                critical_unfixed = critical_unfixed + 1
        elif 'High' in severity:
            high = high + 1
            if fixed == 'fixed':
                high_fixed = high_fixed + 1
            else:
                high_unfixed = high_unfixed + 1
        elif 'Medium' in severity:
            medium = medium +1
            if fixed == 'fixed':
                medium_fixed = medium_fixed + 1
            else:
                medium_unfixed = medium_unfixed + 1
        elif 'Low' in severity:
            low = low + 1
            if fixed == 'fixed':
                low_fixed = low_fixed + 1
            else:
                low_unfixed = low_unfixed + 1
        else:
            unknown = unknown + 1

        if fixed == 'fixed':
            info = "{},{},{},{},{},{},{}".format(container,'grype',id, severity.upper(), installedVersion, fixedVersion, name)
        else:
            info = "{},{},{},{},{},,{}".format(container,'grype',id, severity.upper(), installedVersion, name)

        out_data.append(info)
    print("{:50s}|{:5d}|{:5d}|{:5d}|{:5d}|{:5d}|{:5d}|{:5d}|{:5d}|{:7d}".format(container,critical_fixed,high_fixed,medium_fixed,low_fixed,critical_unfixed,high_unfixed,medium_unfixed,low_unfixed,unknown))
    outfile3.write("{} & {} & {} & {} & {} & {} & {} & {} & {} & {} \\\\ \\hline\n".format(container,critical_fixed,high_fixed,medium_fixed,low_fixed,critical_unfixed,high_unfixed,medium_unfixed,low_unfixed,unknown) )
outfile3.write("\\end{tabular}\n\\caption{Grype - Static Analysis}\\label{table:2}\\end{table}\n\n")


outfile3.write("\\begin{table}[ht!]\n\\centering\n\\begin{tabular}{ |c|c|c| } \\hline\n")
outfile3.write("\\multicolumn{3}{|c|}{Antivirus} \\\\ \\hline\n")
outfile3.write("Container & Scanned & Infected \\\\ \\hline\n")
found_data = "\\begin{table}[ht!]\n\\centering\n\\begin{tabular}{ |c|c| } \\hline\n"
found_data = found_data + "Container & Details \\\\ \\hline\n"
 
for myFile in av:
    container = myFile.split('_av.log')[0].split('static/')[1].split('_')[0]
    scanned = ''
    infected = ''
    infected_list = []

    fileData = open(myFile,'r') 

    for line in fileData:
        if 'Scanned files' in line:
            scanned = line.split(':')[1].strip()
        elif 'Infected files' in line:
            infected = line.split(':')[1].strip()
        elif 'FOUND' in line:
            infected_list.append(line)
    
    data = ""
    for entry in infected_list:
        data = data + entry + "\n"
        
    if len(infected_list) > 1:
        found_data = found_data + "{} & \\multirow\{{}\}{} \\\\ \\hline\n".format(container,len(infected_list),data)
    
    outfile3.write("{} & {} & {} \\\\ \\hline\n".format(container,scanned,infected))  

outfile3.write("\\end{tabular}\n\\caption{Antivirus - Static Analysis}\\label{table:3}\\end{table}\n\n")
found_data = found_data + "\\end{tabular}\n\\caption{Antivirus - Static Analysis Details}\\label{table:4}\\end{table}\n\n"
outfile3.write("\n\n{}\n\n".format(found_data))

for record in out_data:
    outfile.write('{}\n'.format(record))
    outfile2.write('{}\n'.format(record))
