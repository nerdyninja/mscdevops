= Container Security Analysis

== Intro
This repository contains files relevant to my dissertation on Container security tool analysis.
This repository is currently a work in progress, as it is being updated to remove any data / files that are not
relevant moving forward (and will be build from the operation of cleaning up another repository).


Currently you will find the following
- A directory listed VM. This contains files for the creation of a test environment with Hashicorp Packer, Bash and SaltStack
- A directory with the utiliy scripts used in the testing phase.

include::./vm/README.adoc[]

==== Useful Commands
Docker Export (example using MySQL container image)
[source,]
----
docker image save -o mysql.tar mysql
----

Docker Import (example using MySQL container image)
[source,]
----
docker image import -o mysql.tar mysql:tested
----

Rootless Docker Service
[source,]
----
systemctl --user start docker
systemctl --user stop docker
systemctl --user status docker
----
